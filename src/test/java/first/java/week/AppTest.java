package first.java.week;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void TestA()
    {
        assertEquals("A", Yo.setGrade(25,75));
        
    }
    @Test
    public void TestB(){
        assertEquals("B", Yo.setGrade(25, 25));
    }
}
