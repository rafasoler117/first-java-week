package first.java.week.bases;

import java.util.ArrayList;
import java.util.List;

public class Variables {
    
  
    public void first ()
    {
        String namepromo = "Promo16";
        int age = 30;
        String love = "i love JAVA";
        List<String> languageProgra = new ArrayList<>(List.of("Java", "C"));

        System.out.println("my name is :" + namepromo +" i am "+ age +" i am learning :" + languageProgra + " btw " +love);
    }
    
    public void withParamters (String str, int i)
    {
        System.out.println(str + i);
    }

    public String withReturn ()
    {
        return "le Return";
    }
}
