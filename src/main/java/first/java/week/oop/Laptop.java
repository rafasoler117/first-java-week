package first.java.week.oop;



public class Laptop {
    
    private String model;
    private int battery;
    private boolean turnedOn;
    private boolean pluggedIn;
    private String OS;
    

    public Laptop(String model) {
        this.model = model;
        this.battery = 50;
        this.turnedOn = false;
        this.pluggedIn = false;
        this.OS = null;
    }

    public void plug()
    {
        pluggedIn = true;
        consumeBattery();
    }

    public void powerSwitch()
    {
        if (turnedOn == true)
            turnedOn = false;
        if (turnedOn == false && pluggedIn == true)
            turnedOn = true;
        if (turnedOn == false && pluggedIn == true && battery > 10)
            turnedOn = true;
            consumeBattery();
    }
   
   public boolean install(String OS)
   {
        if (turnedOn == true)
        {
            consumeBattery();
            return true;
        }
        else 
            return false;       
   }
   
   private void consumeBattery()
   {
        if (pluggedIn == false)
            battery -= 5;
            else
                battery += 5;
        if (battery == 0)
            turnedOn = false;
        if (battery > 100)
            battery = 100;
   }

public int getBattery() {
    return battery;
}

public void setBattery(int battery) {
    this.battery = battery;
} 
}
